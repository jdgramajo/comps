import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mods-home',
  templateUrl: './mods-home.component.html',
  styleUrls: ['./mods-home.component.css']
})
export class ModsHomeComponent implements OnInit {
  modalOpen = false;
  items = [
    { title: 'Why is the sky blue?', content: 'The sky is blue because it is.' },
    { title: 'What does an orange taste like?', content: 'Because it captured the escense of the Sun.' },
    { title: 'Why does Neo and John Wick look alike?', content: 'Because they are both Keanu.' }
  ];
  openedItemIndex = 0;

  constructor() { }

  ngOnInit() {
  }

  onClick() {
    this.modalOpen = !this.modalOpen;
  }


}
